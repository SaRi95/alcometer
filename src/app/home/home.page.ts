import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  weight: number;
  gender: string;
  time: number;
  bottles: number;
  promilles: number;
  litres: number;
  grams: number;
  burning: number;
  gramsLeft: number;

  constructor() {
    this.weight = 0;
    this.promilles = 0;
    this.litres = 0;
    this.grams = 0;
    this.burning = 0;
    this.gramsLeft = 0;
  }

  calculate() {
    this.litres = this.bottles * 0.33;
    this.grams = this.litres * 8 * 4.5;
    this.burning = this.weight / 10;
    this.gramsLeft = this.grams - (this.burning * this.time);

    if (this.gender === 'm') {
      this.promilles = this.gramsLeft / (this.weight * 0.7);

    } else {
      this.promilles = this.gramsLeft / (this.weight * 0.6);
    }
    //console.log(this.litres,this.grams,this.weight,this.gender,this.burning,this.gramsLeft);
  }
}
